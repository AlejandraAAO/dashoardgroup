package com.example.viewdashboard;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showLineChart(View view) {
        Log.d(LOG_TAG,"ir a ver las lineas");
        //un intent  es como una acción
        Intent intent = new Intent(this, ViewLineChart.class);
        startActivity(intent);
    }

    public void showBarChart(View view) {
        Intent intent = new Intent(this,ViewBarChart.class);
        startActivity(intent);
    }

    public void showStackedBar(View view) {
        Intent intent = new Intent(this, ViewStackedChart.class);
        startActivity(intent);
    }

    public void showPieChart(View view) {
        Intent intent = new Intent(this, ViewPieChart.class);
        startActivity(intent);
    }

    public void showGroupChart(View view) {
        Intent intent = new Intent(this, ViewGroupChart.class);
        startActivity(intent);
    }
}
